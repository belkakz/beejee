<?php 
class ToDoList
{
	public function getAll()
	{
		$pdo = Di::get()->db();
		$sth = $pdo->prepare('SELECT * FROM `tasks` ORDER BY id DESC');
		if ($sth->execute()) {
			return $sth->fetchAll();
		}
		return false;
	}

	public function getOne($id)
	{
		$pdo = Di::get()->db();
		$sth = $pdo->prepare('SELECT * FROM `tasks` WHERE id LIKE ?');
		if ($sth->execute([$id])) {
			return $sth->fetchAll();
		}
		return false;
	}


	public function addTask($data){
		extract($data);
		// print_r($data);
		var_dump($email);
		$pdo = Di::get()->db();
		$sth = $pdo->prepare(
			'INSERT INTO tasks(username, email, task)'
			.'VALUES(:username, :email, :task)');	

		$sth->bindValue(':username', $username, PDO::PARAM_STR);
		$sth->bindValue(':email', $email, PDO::PARAM_STR);
		$sth->bindValue(':task', $task, PDO::PARAM_STR);
		$sth->execute();
		return;
	}

	public function editTask($data){
		$update = [];
		if (!isset($data['status'])) {
			$data['status'] = 0;
		}
		foreach ($data as $param => $value) {
			$update[] = $param.'`=:'.$param;
		}
		extract($data);
		$pdo = Di::get()->db();
		$sth = $pdo->prepare('UPDATE `tasks` SET `'.implode(', `', $update).' WHERE `id`=:id');

		$sth->bindValue(':id', $id, PDO::PARAM_STR);
		$sth->bindValue(':username', $username, PDO::PARAM_STR);
		$sth->bindValue(':email', $email, PDO::PARAM_STR);
		$sth->bindValue(':task', $task, PDO::PARAM_STR);
		$sth->bindValue(':status', $status, PDO::PARAM_INT);
		$sth->execute();		
		if ($sth->execute()) {
			return true;
		} else return false;
		
	}
}
 ?>