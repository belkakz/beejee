<?php 

if (!isset($_GET['c']) || !isset($_GET['a'])) {
    $controller = 'toDoList';
    $action = 'all';
} else {
    $controller = $_GET['c'];
    $action = $_GET['a'];
}


if ($controller == 'toDoList') {
	include 'controller/todolist.php';
	$toDoList = new ToDoListController();
	if ($action == 'all') {
		$toDoList->getAll();
	} elseif ($action == 'add') {
		$toDoList->addTask();
	}  elseif ($action == 'edit') {
		$toDoList -> editTask();
	} 
}


if ($controller == 'admin') {
	include 'controller/adminController.php';
	$adminController = new AdminController();
	if ($action == 'login') {
		$adminController -> login();
	} elseif ($action == 'exit') {
		$adminController -> out();
	}
}


 ?>