<?php 
if (!isAdmin())
	showError403();
 ?>

<div class="container">
	<h1>Редактировать задачу</h1>
	<form action="./?c=toDoList&a=edit" method="POST">		
		<input type="hidden" class="form-control" id="email" aria-describedby="emailHelp" name="id" value="<?=$task['id']?>" readonly>
		<div class="form-group">
			<label for="email">Email address</label>
			<input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email" value="<?=$task['email']?>" readonly>
		</div>
		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" class="form-control" id="username" name="username" value="<?=$task['username']?>" readonly>
		</div>
		<div class="form-group">
			<label for="task">task</label>
			<textarea class="form-control" rows="3" id="task" name="task"><?=$task['task']?></textarea>
		</div>
		<div class="form-check form-check-inline">
			<?php if ($task['status']): ?>
				<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="1" name="status" checked>
			<?php else: ?>
				<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="1" name="status">
			<?php endif ?>			
			<label class="form-check-label" for="inlineCheckbox1">Выполнена</label>
		</div>
		<br>
		<br>
		<button type="submit" class="btn btn-primary">Редактировать</button>
	</form>
</div>