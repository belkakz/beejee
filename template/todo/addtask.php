<div class="container">
	<h1>Добавить задачу</h1>
	<form action="./?c=toDoList&a=add" method="POST">
		<div class="form-group">
			<label for="email">Email address</label>
			<input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email">
		</div>
		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" class="form-control" id="username" name="username">
		</div>
		<div class="form-group">
			<label for="task">task</label>
			<textarea class="form-control" rows="3" id="task" name="task"></textarea>
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
</div>