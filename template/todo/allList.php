<?php 
// print_r($allList);
 ?>
<div class="container">
	<h1>Список задач</h1>
	<div class="top">
		<a href="?c=toDoList&a=add" class="add btn btn-primary">Добавить задачу +</a>
		<?php if (isAdmin()): ?>
			<a href="?c=admin&a=exit" class="login btn btn-primary">LogOut</a>	
		<?php else: ?>
			<a href="?c=admin&a=login" class="login btn btn-primary">Login</a>
		<?php endif ?>
	</div>
	<table class="table table-hover dataTable ">
		<tr>
			<th>Пользователь</th>
			<th>email</th>
			<th>задача</th>
			<th>статус</th>
			<?php if (isAdmin()): ?>
				<th>действие</th>
			<?php endif ?>
		</tr>
		<?php foreach ($allList as $key => $task): ?> 	
		<tr>
			<td><?=$task['username']?></td>
			<td><?=$task['email']?></td>
			<td><?=$task['task']?></td>
			<td>
				<?php 
					switch ($task['status']) {
						case 1:
							echo "Выполнена";
							break;
						
						default:
							echo "В процессе";
							break;
					}
				 ?>
			</td> 	
			<?php if (isAdmin()): ?>
				<th><a href="./?c=toDoList&a=edit&id=<?=$task['id']?>">Редактировать</a></th>
			<?php endif ?>	
		</tr>	
		<?php endforeach ?>
	</table>	
</div>