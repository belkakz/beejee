<?php 
include 'model/todolist.php';

class ToDoListController 
{
	 /**
     * Получение всех записей
     */
	public function getAll() 
	{
		$toDoList = new toDoList();
		$allList = $toDoList -> getAll();
		Di::get()->render('todo/alllist.php', ['allList' => $allList]);

	}

	public function addTask() 
	{
		if (count($_POST) > 0) {			
			$toDoList = new toDoList();
			$addTask = $toDoList -> addTask($_POST);
				echo '<script>
						alert("Дело успешно добавлено")
						window.location.href = "index.php"
					</script>';
		} else {
			Di::get()->render('todo/addtask.php');
		}		
	}

	public function editTask() 
	{		
		$toDoList = new toDoList();
		$oneTask = $toDoList -> getOne($_GET['id']);
		if (count($_POST) > 0) {	
			$editTask = $toDoList -> editTask($_POST);
				echo '<script>
						alert("Дело успешно Отредактировано")
						window.location.href = "index.php"
					</script>';
		} else {
			Di::get()->render('todo/edittask.php', ['task' => $oneTask[0]]);
		}		
	}

	public function out()
	{
		session_destroy();	
		header ('location: ./');
	}

}

 ?>