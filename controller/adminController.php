<?php 
include 'model/admin.php';
class AdminController
{
	public function login()
	{
		if ((!isset($_SESSION['role'])) && ($_SESSION['role'] !== 'admin')) {
			$admin = new Admin();
			if (count($_POST) > 0) {
				$login = $admin->login($_POST['login']);
				if (!empty($login)) {
					print_r($_POST['pass']);
					$login = $login[0];
					if ($_POST['pass'] === $login['password']) {	
						$_SESSION['role'] = 'admin';
						$_SESSION['user_id'] = $login['id'];
						header ('location: /');
						echo "Удачный вход";
					} 
				}
				echo "Пароль или логин неверный!";
			}
			Di::get()->render('admin/login.php');
		}	
	}

	public function out()
	{
		session_destroy();	
		header ('location: /');
	}


}

 ?>